package in.ac.iitb.ivrs.telephony.base.util;

import in.ac.iitb.ivrs.telephony.base.IVRSession;
import in.ac.iitb.ivrs.telephony.base.IVRSessionFactory;
import in.ac.iitb.ivrs.telephony.base.config.IVRConfigs;
import in.ac.iitb.ivrs.telephony.base.config.IVRConfigs.KooKoo;
import in.ac.iitb.ivrs.telephony.base.config.IVRConfigs.Network;
import in.ac.iitb.ivrs.telephony.base.events.DisconnectEvent;
import in.ac.iitb.ivrs.telephony.base.events.GotDTMFEvent;
import in.ac.iitb.ivrs.telephony.base.events.NewCallEvent;
import in.ac.iitb.ivrs.telephony.base.events.RecordEvent;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Authenticator;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.PasswordAuthentication;
import java.net.Proxy;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Map;

import javax.servlet.http.HttpSession;

/**
 * Telephony-specific utility functions.
 */
public class IVRUtils {

	/**
	 * Internal class to hold status information of a GET/POST response.
	 */
	private static class ConnectionStatus {
		/**
		 * The response code of the attempt.
		 */
		public int responseCode;
		/**
		 * The data that was received.
		 */
		public String data;

		public ConnectionStatus(int responseCode, String data) {
			this.responseCode = responseCode;
			this.data = data;
		}
	}

	/**
	 * Handles an incoming IVR request from the telephony provider (KooKoo).
	 * @param parameterMap The request parameter map.
	 * @param httpSession The HTTP session associated with this request.
	 * @param sessionFactory An object that can create new application-specific IVR sessions.
	 * @return A string to be sent back to the server as a response.
	 * @throws Exception
	 */
	public static String doCallHandling(Map<String, String[]> parameterMap, HttpSession httpSession,
			IVRSessionFactory sessionFactory) throws Exception {

		String event = parameterMap.get("event")[0];
		String sessionId = parameterMap.get("sid")[0];
		if (event.equals("NewCall")) {
			String userNumber = parameterMap.get("cid")[0].replaceAll("[^0-9]", "");
			if(userNumber==null|| userNumber.length()<=11)
			{ 
				if(userNumber.charAt(0)=='0'&&userNumber.length()==11)
				{
					userNumber=userNumber.substring(1);
				}
				if(userNumber.length()==10)
				{
					userNumber="91"+userNumber;
				}
			}
			String ivrNumber = parameterMap.get("called_number")[0];
			String circle = parameterMap.get("circle")[0];
			String operator = parameterMap.get("operator")[0];

			IVRSession session = sessionFactory.createSession(sessionId, userNumber, ivrNumber, circle, operator);
			httpSession.setAttribute("telephony", session);

			session.doEvent(new NewCallEvent());
			return session.flushResponse();

		} else {
			IVRSession session = (IVRSession) httpSession.getAttribute("telephony");

			if (event.equals("GotDTMF")) {
				String data = parameterMap.get("data")[0];
				session.doEvent(new GotDTMFEvent(data));

			} else if (event.equals("Record")) {
				String data = parameterMap.get("data")[0];
				int duration = Integer.parseInt(parameterMap.get("record_duration")[0]);
				session.doEvent(new RecordEvent(data, duration));

			} else if (event.equals("Hangup") || event.equals("Disconnect")) {
				int duration = Integer.parseInt(parameterMap.get("total_call_duration")[0]);
				session.doEvent(new DisconnectEvent(duration));
				httpSession.invalidate();
			}

			return session.flushResponse();
		}
	}

	/**
	 * Connects to a given URL. This function also transparently handles connecting to a proxy if required.
	 * @param url The URL to connect to.
	 * @param postURLParams The URL parameters if the request should be POST. Keep this null for a GET request.
	 * @return A ConnectionStatus object containing the result of the URL request.
	 * @throws IOException 
	 */
	private static ConnectionStatus connectToURL(String url, String postURLParams) throws IOException {
		URL obj = new URL(url);
		HttpURLConnection con = null;

		System.out.println("Sending request to: " + url + " (POST params: " + postURLParams + ")");

		// specify the proxy parameters if a proxy is needed.
		if (IVRConfigs.Network.USE_PROXY) {
			Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(IVRConfigs.Network.PROXY_HOST, IVRConfigs.Network.PROXY_PORT));
			Authenticator.setDefault(new Authenticator() {
				@Override
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(IVRConfigs.Network.PROXY_USER, IVRConfigs.Network.PROXY_PASS.toCharArray());
				}
			});
			con = (HttpURLConnection) obj.openConnection(proxy);
		} else {
			con = (HttpURLConnection) obj.openConnection();
		}

		con.setRequestProperty("User-Agent", "Mozilla/5.0"); // I guess this doesn't need to be changed.. --Ankit

		if (postURLParams == null) {
			con.setRequestMethod("GET");
		} else {
			con.setRequestMethod("POST");
			con.setDoOutput(true);
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(postURLParams);
			wr.close();
		}

		int responseCode = con.getResponseCode();

		BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer resp = new StringBuffer();
		while ((inputLine = in.readLine()) != null) {
			resp.append(inputLine);
		}
		in.close();

		return new ConnectionStatus(responseCode, resp.toString());
	}

	/**
	 * Make an outbound call.
	 * @param phoneNo The phone number to call.
	 * @param ivrNumber The IVR number to call from.
	 * @param appUrl The telephony application URL that KooKoo should call.
	 * @return true if the outbound call request succeeded, false otherwise. 
	 */
	public static boolean makeOutboundCall(String phoneNo, String ivrNumber, String appUrl) {
		try {
			ConnectionStatus status = connectToURL("http://www.kookoo.in/outbound/outbound.php?phone_no=" + phoneNo
					+ "&api_key=" + IVRConfigs.KooKoo.API_KEY + "&outbound_version=2&caller_id=" + ivrNumber + "&url="
					+ appUrl + "&callback_url=" + appUrl, null);

			System.out.println("Outbound call request: " + status.responseCode + ": " + status.data);
			if (status.responseCode == 200 && status.data.contains("queued"))
				return true;
			else
				return false;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * Send an SMS.
	 * @param phoneNo The phone number to send SMS to.
	 * @param message The text message.
	 * @param senderId The 6-character sender ID to send the message as.
	 * @param callbackUrl The callback URL.
	 * @return true if the message was sent, false otherwise. 
	 */
	public static boolean sendSMS(String phoneNo, String message, String senderId, String callbackUrl) {
		try {
			String senderPart = (senderId != null) ? ("&senderid=" + senderId) : "";
			String callbackPart = (callbackUrl != null) ? ("&callback=" + callbackUrl) : "";
			ConnectionStatus status = connectToURL("http://kookoo.in/outbound/outbound_sms.php?phone_no=" + phoneNo
					+ "&api_key=" + IVRConfigs.KooKoo.API_KEY + "&unicode=true&message="
					+ URLEncoder.encode(message, "UTF-8") + senderPart + callbackPart, null);

			System.out.println("Outbound call request: " + status.responseCode + ": " + status.data);
			if (status.responseCode == 200 && status.data.contains("success"))
				return true;
			else
				return false;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * Adds a new audio file to the KooKoo audio cache.
	 * @param url The audio URL to cache.
	 * @return true if the operation succeeded, false otherwise.
	 */
	public static boolean addAudioToCache(String url) {
		try {
			ConnectionStatus status = connectToURL("http://kookoo.in/restkookoo/index.php/api/cache/audio/", "api_key="
					+ IVRConfigs.KooKoo.API_KEY + "&url=" + url);

			System.out.println("Add audio to cache: " + status.responseCode + ": " + status.data);
			if (status.responseCode == 200 && status.data.contains("success"))
				return true;
			else
				return false;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * Check if a given URL is cached.
	 * @param url The audio URL to check.
	 * @return true if the given URL is in the cache, false otherwise.
	 */
	public static boolean checkifCached(String url) {
		try {
			String encodedUrl = URLEncoder.encode(url.replace('/', '\\'), "UTF-8");
			ConnectionStatus status = connectToURL("http://kookoo.in/restkookoo/index.php/api/cache/audio/api_key/" + IVRConfigs.KooKoo.API_KEY
					+ "/url/" + encodedUrl, null);

			System.out.println("Check audio in cache: " + status.responseCode + ": " + status.data);
			if (status.responseCode == 200 && !status.data.contains("error"))
				return true;
			else
				return false;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * Removes an audio file from the KooKoo audio cache.
	 * @param url The audio URL to remove from cache.
	 * @return true if the operation succeeded, false otherwise.
	 */
	public static boolean deleteCachedAudio(String url) {
		try {
			String encodedUrl = URLEncoder.encode(url.replace('/', '\\'), "UTF-8");
			ConnectionStatus status = connectToURL("http://kookoo.in/restkookoo/index.php/api/cache/audio/api_key/" + IVRConfigs.KooKoo.API_KEY
					+ "/url/" + encodedUrl + "/delete/true", null);

			System.out.println("Delete audio from cache: " + status.responseCode + ": " + status.data);
			if (status.responseCode == 200 && status.data.contains("success"))
				return true;
			else
				return false;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * Clears the KooKoo audio cache.
	 * @return true if the operation succeeded, false otherwise.
	 */
	public static boolean clearCache() {
		try {
			ConnectionStatus status = connectToURL("http://kookoo.in/restkookoo/index.php/api/cache/audio/api_key/"
					+ IVRConfigs.KooKoo.API_KEY + "/delete/all", null);

			System.out.println("Clear audio cache: " + status.responseCode + ": " + status.data);
			if (status.responseCode == 200 && status.data.contains("success"))
				return true;
			else
				return false;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * Testing the utils.
	 */
	public static void main(String[] args) throws IOException {
		// set config for testing
		KooKoo.API_KEY = "KK4063496a1784f9f003768bd6e34c185b";
		Network.USE_PROXY = true;
		Network.PROXY_HOST = "netmon.iitb.ac.in";
		Network.PROXY_PORT = 80;
		Network.PROXY_USER = "p14293";
		Network.PROXY_PASS = "krishna*";
		String url = "http://ruralict.cse.iitb.ac.in/Downloads/voices/welcomeMessage/welcomeMessage2_en.wav";
		//System.out.println(checkifCached(url));
		//String url = "http://qassist.cse.iitb.ac.in/Downloads/AFC/audio-upload/Welcome_message2015-01-05-21:05:32.wav";
		//System.out.println(addAudioToCache(url)); // add this URL to cache, should work
		//System.out.println(checkifCached(url)); // check if URL is in cache, should work
		System.out.println(clearCache()); // clear the cache, should work
		//System.out.println(deleteCachedAudio(url)); // delete URL from cache, this should fail
		//System.out.println(checkifCached(url)); // check if URL is in cache, this should fail
	}

}